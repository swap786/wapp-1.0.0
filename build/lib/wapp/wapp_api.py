from yowsup.demos import sendclient
from yowsup.registration import WACodeRequest
from yowsup.registration import WARegRequest
from yowsup.demos import contacts

from flask import jsonify
from dbconnect import insert_data , extract
from subprocess import check_output

def send(phone_no,password,reciver,message):
	receivers = []	
	r = reciver.split(',')
	for i in r:
		receivers.append([str(i) , str(message)])

	creds = (str(phone_no),str(password),)
	print receivers

	try:
		s = sendclient.YowsupSendStack(creds , receivers,False)
		s.start()
	except :
		return "sent"
	return 'something went wrong'


def register_no(cc, p_in, mcc, mnc,sim_mcc, sim_mnc):
	try:
		codeReq = WACodeRequest(cc, p_in, mcc, mnc,sim_mcc, sim_mnc)
		result = codeReq.send()
		if result["pw"]:
			insert_data(p_in,result["pw"],cc)
		return jsonify(result)
	except:
		return "<html><center><h3>Problem; Check Your Internet Connection.</h3></center></html>"

def register_code(cc,p_in,code):
	try:
		req = WARegRequest(cc,p_in,code)
	        result = req.send()
		print result["pw"]
		if result["pw"]:
			insert_data(p_in,result["pw"],cc)
		return jsonify(result)
	except:
		return "<html><center><h3>Problem; Check Your Internet Connection Or SMS Code is wrong.</h3></center></html>"

 
def validate_contact(phone_no,contact_list):
	credentials = extract(phone_no)
	try:
		if credentials:
			out = check_output(['./contact_sync.py',str(credentials[2])+phone_no,credentials[1],contact_list])
			#print out
			out = out.replace('[','')
			out = out.replace(']','')
			out = out.replace("'",'')
			i = out.rfind('In Numbers:')
			k = out.rfind('Out Numbers:')
			j = out.rfind('Invalid Numbers:')
			
			in_numbers = out[i:k]
			out_numbers = out[k:j]
			return in_numbers+"<br/>"+out_numbers
		else:
			return "<html><center><h3>Problem; Phone No: "+phone_no+" Not Registered.</h3></center></html>"
	except:
		return "<html><center><h3>Problem; Check Your Internet Connection.</h3></center></html>"

def sendMessage(phone_no,reciver,message):
	credentials = extract(phone_no)
	if credentials:
		return  send(str(credentials[2])+phone_no,credentials[1],reciver,message)
	else:
		return "register number"
	

