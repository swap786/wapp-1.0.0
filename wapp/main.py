from flask import Flask , redirect, request
from flask import g, redirect, url_for
from flask import abort, render_template, flash
from wapp_api import sendMessage,validate_contact
from wapp_api import register_code as r_code ,register_no


app = Flask(__name__)
@app.route('/')
def index():
	return render_template('home.html')

@app.route('/register_number')
def render_register():
	return render_template('new_register.html')


@app.route('/msg_send')
def render_send():
	return render_template('msg_send.html')

@app.route('/sync_contacts')
def render_sync():
	return render_template('contact_sync.html')

@app.route('/register_code_sms')
def render_sms_code():
	return render_template('sms_code.html')


@app.route('/send',methods=['POST'])
def send():
	phone_no = request.form['phone_no']
	reciver = request.form['receiver']
	message = request.form['message']
	sendMessage(phone_no,reciver,message)
	return redirect('/msg_send')

@app.route('/sync',methods=['POST'])
def sync():
	if request.form['phone_no'] and request.form['contacts']:
		phone_no = request.form['phone_no']
		contact_list = request.form['contacts']
		return validate_contact(phone_no,contact_list)
	else:
		return "<html><center><h3>Empty Text Fields.</h3></center></html>"

@app.route('/register_code',methods=['POST'])
def register_code():
	if request.form['cc'] and request.form['phone_no'] and request.form['code']:
		cc = request.form['cc']
		phone_no = request.form['phone_no']
		code = request.form['code']
		return r_code(str(cc),str(phone_no),str(code))
	else:
		return "<html><center><h3>Empty Text Fields.</h3></center></html>"

@app.route('/register',methods=['POST'])
def register():
	if request.form['cc'] and request.form['phone_no'] and request.form['mcc'] and request.form['mnc']:

		return register_no(request.form['cc'], request.form['phone_no'], request.form['mcc'], request.form['mnc'], request.form['mcc'], request.form['mnc'])
	
	else:
		return "<html><center><h3>Empty Text Fields.</h3></center></html>"

if __name__ == '__main__':
	app.run(debug=True)
