#!/usr/bin/env python
import sys
from yowsup.demos import contacts
import logging
import pickle
import xml.etree.ElementTree as ET

LOG_FILENAME = 'contacts.log'

logging.basicConfig(filename=LOG_FILENAME,level = logging.DEBUG)

logging.debug('This message should go to the log file')	

def sync(credentials,contactlist):
	with open(LOG_FILENAME, 'w'): pass
	with open('contacts.txt', 'w'): pass
	valid_contacts = []
	contactlist_plus = []
	for tmp in contactlist.split(','):
		contactlist_plus.append('+'+tmp)
	try:
		stack = contacts.YowsupSyncStack(credentials,contactlist_plus,False)
		stack.start()
	except KeyboardInterrupt:
		with open(LOG_FILENAME, 'r') as content_file:
			content = content_file.read()

		i = content.rfind('<iq type="result" from="'+credentials[0]+'@s.whatsapp.net"')
		k = content.rfind('</iq>')
		data = content[i:k+5]
	
		tree = ET.fromstring(data)
	
		for in_tag in tree.findall('.//in'):
			for user in in_tag.findall('.//user'):
		    		jid = user.get('jid')
		    		valid_contacts.append(jid.split('@')[0])

		vc = str(valid_contacts).replace('[','')
		vc = vc.replace(']','')
		vc = vc.replace(' ','')
		vc = vc.replace("'",'')
		with open("contacts.txt", "a") as myfile:
			myfile.write(str(vc))
		#print str(valid_contacts)
	except Exception, e:
                print "Exce: "+str(e)

try:
        creds = (sys.argv[1],sys.argv[2],)      
        #f=open('f1.txt','r')    
        contactlist = sys.argv[3]
        sync(creds,contactlist)
except Exception, e:
        print "Except: "+str(e)                
