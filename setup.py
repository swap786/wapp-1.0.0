#!/usr/bin/env python
from __future__ import print_function
from setuptools import setup, find_packages
import platform

deps = ['python-dateutil', 'argparse', 'python-axolotl>=0.1.7', 'pillow','Flask==0.10.1','watchdog','python-dateutil']

if platform.system().lower() == "windows":
    deps.append('pyreadline')
else:
    try:
        import readline
    except ImportError:
        deps.append('readline')
setup(
    name='wapp',
    version="1.0.0",
    description='OpenShift App',
    author='Your Name',
    author_email='example@example.com',
    url='rhcloud.com',
    install_requires =deps,
    packages= find_packages(),
    include_package_data=True,
    scripts = ['wapp/contact_sync.py','wapp/main.py'],
    data_files=[('database', ['wapp/contacts.db'])],
    )
